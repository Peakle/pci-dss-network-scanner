<?php

namespace App\Command;

use App\Service\ScanService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected static $defaultName = 'app:test';
    protected $service;
    protected $socket;

    public function __construct(ScanService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Command for test features');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->socket = ssh2_connect("192.168.1.10", "32776");
        unset($this->socket);
        dd($this->socket);

    }
}
