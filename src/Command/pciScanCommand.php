<?php

namespace App\Command;

use \App\Service\ScanService;
use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class pciScanCommand extends Command
{

    protected $service;
    protected static $defaultName = 'pci:scan';

    public function __construct(ScanService $service)
    {
        $this->service = new $service(3);
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Команда проводит сканирование хоста.');
        $this->addArgument('host', InputArgument::REQUIRED, "адрес удаленного хоста");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hosts_range = $input->getArgument('host');
        $results = [];

        $hosts = $this->getEachIpInRange($hosts_range);

        foreach ($hosts as $host) {
            //скан портов
            $this->service->scanPorts($host, $results);

            if (isset($results[$host]) && $results[$host]) {

                $this->service->processOpenPorts($results);
            }
        }

        dump($results);
    }

    private function getIpRange(&$cidr): array
    {
        $ip_mask = explode('/', $cidr);

        if (count($ip_mask) >= 2) {

            list($ip, $mask) = $ip_mask;
            $maskBinStr = str_repeat("1", $mask) . str_repeat("0", 32 - $mask);      //net mask binary string
            $inverseMaskBinStr = str_repeat("0", $mask) . str_repeat("1", 32 - $mask); //inverse mask

            $ipLong = ip2long($ip);
            $ipMaskLong = bindec($maskBinStr);
            $inverseIpMaskLong = bindec($inverseMaskBinStr);
            $netWork = $ipLong & $ipMaskLong;

            $start = $netWork + 1;//ignore network ID(eg: 192.168.1.0)

            $end = ($netWork | $inverseIpMaskLong) - 1; //ignore broadcast IP(eg: 192.168.1.255)
            return ['firstIP' => $start, 'lastIP' => $end];
        } elseif ($ip_mask) {
            $ip = ip2long(array_shift($ip_mask));
            return ['firstIP' => $ip, 'lastIP' => $ip];
        }
    }

    private function getEachIpInRange(&$cidr): iterable
    {
        $range = $this->getIpRange($cidr);
        for ($ip = $range['firstIP']; $ip <= $range['lastIP']; $ip++) {
            yield long2ip($ip);
        }
    }
}