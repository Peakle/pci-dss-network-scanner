<?php

namespace App\Service;

class ScanService
{
    protected $socket;
    protected $timeout;
    protected $try_num;

    public function __construct(int $timeout = 2)
    {
        $this->timeout = $timeout;
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    private function disconnect(): void
    {
        if (is_resource($this->socket)) {
            @fclose($this->socket);
            @ssh2_disconnect($this->socket);
            @ftp_close($this->socket);
        }
    }

    /**
     * @param $host
     * @param $results
     */
    public function scanPorts(&$host, &$results)
    {
        $ports = [
            'ftp' => 21,
            'ssh' => 22,
            'telnet' => 23,
            'http' => 80,
            'https' => 443,
        ];

        $open_ports = [];

        foreach ($ports as $protocol => $port) {

            $this->socket = @fsockopen($host, $port, $errno, $errstr, $this->timeout);

            if (is_resource($this->socket)) {
                $open_ports[$port] = ['protocol' => $protocol];
                fclose($this->socket);
            }
        }

        $results[$host] = $open_ports;
    }

    /**
     * @param $results
     */
    public function processOpenPorts(&$results)
    {
        foreach ($results as $hostname => $open_ports) {
            foreach ($open_ports as $port => $port_info) {

                switch ($port_info['protocol']) {

                    case "ssh":
                        $this->checkSSH($hostname, $port, $results[$hostname][$port]['credentials']);
                        break;

                    case "telnet":
                        //TODO
//                        $this->checkTelnet($hostname, $port, $open_ports[$port]['credentials']);
                        break;

                    case "https":
                    case "http":
                        $this->checkHTTP($hostname, $port, $open_ports[$port]['credentials']);
                        break;

                    case "ftp":
                        $this->checkFTP($hostname, $port, $open_ports[$port]['credentials']);
                        break;
                }
            }
        }
    }

    /**
     * @param $host
     * @param $port
     * @param $result
     */
    public function checkHTTP(&$host, &$port, &$result): void
    {
        $ch = curl_init($host);

        curl_setopt_array($ch, [
            CURLOPT_HEADER => true,
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_PORT => $port,
        ]);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 401) {

            $this->checkHTTPAuthMethod($response, $http_auth_method);

            $credentials = $this->credentialsGenerator();

            foreach ($credentials as $user_pass) {

                $login = $user_pass['login'];
                $pass = $user_pass['password'];

                $user_pass = $login . ":" . $pass;

                if ($this->loginHTTP($host, $port, $user_pass, $http_auth_method)) {

                    $result[] = [
                        'login' => $login,
                        'password' => $pass
                    ];
                }
            }
        }
    }

    /**
     * @param $response
     * @param $http_auth_method
     */
    private function checkHTTPAuthMethod(&$response, &$http_auth_method): void
    {
        preg_match("/WWW-Authenticate: Basic/", $response, $matches);

        if ($matches) {
            $http_auth_method = CURLAUTH_BASIC;
            return;
        }

        unset($matches);

        preg_match("/WWW-Authenticate: Digest/", $response, $matches);

        if ($matches) {
            $http_auth_method = CURLAUTH_DIGEST;
            return;
        }
    }

    /**
     * @param $host
     * @param $port
     * @param $user_pass
     * @param $http_auth_method
     * @return bool
     */
    public function loginHTTP(&$host, &$port, &$user_pass, &$http_auth_method)
    {
        $ch = curl_init($host);

        curl_setopt_array($ch, [
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_PORT => $port,
            CURLOPT_HTTPAUTH => $http_auth_method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERPWD => $user_pass,
            CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
        ]);

        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($code == 200) {
            return true;
        }

        return false;
    }

    /**
     * @param $host
     * @param $port
     * @param $result
     */
    public function checkSSH(&$host, &$port, &$result): void
    {
        $this->socket = ssh2_connect($host, $port);

        if (is_resource($this->socket)) {

            $credentials = $this->credentialsGenerator();

            foreach ($credentials as $user_pass) {
                $login = $user_pass['login'];
                $pass = $user_pass['password'];

                if (@ssh2_auth_password($this->socket, $login, $pass) === true) {

                    $result[] = [
                        'login' => $login,
                        'password' => $pass
                    ];
                }

                ssh2_disconnect($this->socket);
                $this->socket = ssh2_connect($host, $port);
            }
        }
    }

    /**
     * @param $host
     * @param $port
     * @param $result
     */
    public function checkFTP(&$host, &$port, &$result): void
    {
        $this->socket = ftp_connect($host, $port);

        if (is_resource($this->socket)) {

            $credentials = $this->credentialsGenerator();

            foreach ($credentials as $user_pass) {
                $login = $user_pass['login'];
                $pass = $user_pass['password'];

                if (@ftp_login($this->socket, $login, $pass) === true) {

                    $result[] = [
                        'login' => $login,
                        'password' => $pass
                    ];
                }

                ftp_close($this->socket);
                $this->socket = ftp_connect($host, $port);
            }
        }
    }

    /**
     * @return iterable
     */
    public function credentialsGenerator(): iterable
    {
        $users = [
            1 => 'admin', 2 => 'root', 3 => 'administrator'
        ];

        $passwords = [
            1 => 'password', 2 => 'root', 3 => 'admin', 4 => '123456', 5 => '1234', 6 => 'PASSWORD', 7 => 'Password', 8 => "12345678"
        ];


        foreach ($users as $num_user => $user) {

            foreach ($passwords as $num_pass => $password) {

                yield [
                    'login' => $user,
                    'password' => $password
                ];
            }
        }
    }

    /**
     * @param $message
     * @return bool|int
     */
    public function write($message)
    {
        $response = fwrite($this->socket, $message, 1024);

        return $response;
    }
}